import React from 'react'
import {Button} from 'react-bootstrap'

export default class DivTwo extends React.Component {
  constructor(props){
    super(props)
    this.state = {number: props.number}
  }

  divTwo(){
    this.setState({number: this.state.number/2})
  }


  render(){
    return(
      <div style={{color:'#ff9900', fontSize: "30px"}}>
        {this.state.number}
        <br/>
        <Button bsStyle="warning" style={{color:'#990099'}} onClick={this.divTwo.bind(this)}>Div 2</Button>
      </div>
    )
  }

}
